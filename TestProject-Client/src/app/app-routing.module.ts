import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './components/account/signin/signin.component';
import { SignupComponent } from './components/account/signup/signup.component';
import { SignInGuardService } from './services/guards/account.guard';
import { UsersComponent } from './components/users-management/users/users.component';
import { EditUserComponent } from './components/users-management/edit-user/edit-user.component';
import { AddUserComponent } from './components/users-management/add-user/add-user.component';
import {PermissionGuard} from './services/guards/permission.guard';
import {PermissionsConfig} from './services/http/services/account/permissions/permissions.config';
import {StudentsComponent} from './components/students/students/students.component';
import {AddStudentComponent} from './components/students/add-student/add-student.component';
import {EditStudentComponent} from './components/students/edit-student/edit-student.component';

const routes: Routes = [
  { path: 'signin', component: SigninComponent },
  { path: '', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'back-office', canActivate: [SignInGuardService], children: [
    { path: 'users', children: [
      { path: '', canActivate: [PermissionGuard], component: UsersComponent, data: {mandatoryPermission: PermissionsConfig.ViewUsers} },
      { path: 'edit/:userId', canActivate: [PermissionGuard], component: EditUserComponent, data: {mandatoryPermission: PermissionsConfig.EditUsers, title: 'Heroes List'} },
      { path: 'add', canActivate: [PermissionGuard], component: AddUserComponent, data: {mandatoryPermission: PermissionsConfig.AddUsers }},
    ]},
    { path: 'students', children: [
      { path: '', canActivate: [PermissionGuard], component: StudentsComponent, data: {mandatoryPermission: PermissionsConfig.ViewStudents }},
      { path: 'edit/:studentId', canActivate: [PermissionGuard], component: EditStudentComponent, data: {mandatoryPermission: PermissionsConfig.EditStudents }},
      { path: 'add', canActivate: [PermissionGuard], component: AddStudentComponent, data: {mandatoryPermission: PermissionsConfig.AddStudents }},
    ]},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

