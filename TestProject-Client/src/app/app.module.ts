import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoaderComponent } from './shared/loader/loader.component';
import { FormBuilderComponent } from './shared/dynamic-form/form-builder/form-builder.component';
import { MaterialModule } from './material.module';
import { SignupComponent } from './components/account/signup/signup.component';
import { SigninComponent } from './components/account/signin/signin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccountService } from './services/http/services/account/account.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule } from '@auth0/angular-jwt';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from './shared/dynamic-form/elements/button/button.component';
import { CheckboxComponent } from './shared/dynamic-form/elements/checkbox/checkbox.component';
import { InputComponent } from './shared/dynamic-form/elements/input/input.component';
import { RadioButtonComponent } from './shared/dynamic-form/elements/radio-button/radio-button.component';
import { DynamicFieldDirective } from './shared/dynamic-form/dynamic-field/dynamic-field.directive';
import { SignInGuardService } from './services/guards/account.guard';
import { DataTableComponent } from './shared/data-table/data-table.component';
import { AddUserComponent } from './components/users-management/add-user/add-user.component';
import { EditUserComponent } from './components/users-management/edit-user/edit-user.component';
import { UsersComponent } from './components/users-management/users/users.component';
import {LocalStorageService} from './services/local-storage/local-storage.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {InformationBarService} from './services/informational-bar/information-bar.service';
import {NavigationComponent} from './shared/navigation/navigation.component';
import {HttpErrorInterceptor} from './services/http/interceptor/http-error.interceptor';
import {JwtAuthorizationInterceptor} from './services/http/interceptor/jwt-authorization.interceptor';
import {SelectComponent} from './shared/dynamic-form/elements/select/select.component';
import {EditStudentComponent} from './components/students/edit-student/edit-student.component';
import {StudentsComponent} from './components/students/students/students.component';
import {AddStudentComponent} from './components/students/add-student/add-student.component';
import {DateComponent} from './shared/dynamic-form/elements/date/date.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    AddUserComponent,
    EditUserComponent,
    UsersComponent,
    DataTableComponent,
    NavigationComponent,
    LoaderComponent,
    ButtonComponent,
    SelectComponent,
    StudentsComponent,
    EditStudentComponent,
    CheckboxComponent,
    InputComponent,
    DateComponent,
    FormBuilderComponent,
    RadioButtonComponent,
    AddStudentComponent,
    DynamicFieldDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        whitelistedDomains: ['example.com'],
        blacklistedRoutes: ['example.com/examplebadroute/']
      }
    })
  ],
  providers: [
    AccountService,
    SignInGuardService,
    JwtHelperService,
    LocalStorageService,
    InformationBarService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtAuthorizationInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

