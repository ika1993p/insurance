import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilderComponent } from 'src/app/shared/dynamic-form/form-builder/form-builder.component';
import { IFieldConfig } from 'src/app/shared/dynamic-form/form-builder/IFieldConfig';
import {AccountService} from '../../../services/http/services/account/account.service';
import { SigninFieldsConfig } from 'src/app/config/form-configs/account/signin-fields.config';
import {UserSigninModel} from '../../../config/http/response-data-models/user-signin.model';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent {
  @ViewChild(FormBuilderComponent) form: FormBuilderComponent;

  public isLoading = false;

  public regConfig: IFieldConfig[] = SigninFieldsConfig;

  public constructor(private authenticatorService: AccountService,
                     private router: Router,
                     private localStorage: LocalStorageService) { }

  public submit(value: any): void {
    this.isLoading = true;
    this.authenticatorService.signIn(value)
      .subscribe((data: UserSigninModel) => this.userLoggedIn(data));
  }

  public userLoggedIn(user: UserSigninModel) {
    this.isLoading = false;
    this.localStorage.add('user', JSON.stringify(user));
    this.router.navigate(['back-office/students']);
  }
}
