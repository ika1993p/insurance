import { Component, ViewChild } from '@angular/core';
import { FormBuilderComponent } from 'src/app/shared/dynamic-form/form-builder/form-builder.component';
import { IFieldConfig } from 'src/app/shared/dynamic-form/form-builder/IFieldConfig';
import {AccountService} from '../../../services/http/services/account/account.service';
import {SignupFieldsConfig} from '../../../config/form-configs/account/signup-fields.config';
import {UserCreateModel} from '../../../config/http/request-data-models/user-create.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  @ViewChild(FormBuilderComponent) form: FormBuilderComponent;

  public isLoading = false;

  public regConfig: IFieldConfig[] = SignupFieldsConfig;

  public constructor(private authenticatorService: AccountService,
                     private router: Router) { }

  public submit(value: UserCreateModel): void {
    this.isLoading = true;
    this.authenticatorService.signUp(value).subscribe(() => this.userSignedUp());
  }

  public userSignedUp(): void {
    this.isLoading = false;
    this.router.navigate(['signin']);
  }
}
