import {Component, ViewChild} from '@angular/core';
import {FormBuilderComponent} from '../../../shared/dynamic-form/form-builder/form-builder.component';
import {IFieldConfig} from '../../../shared/dynamic-form/form-builder/IFieldConfig';
import {StudentsManagementService} from '../../../services/http/services/students/students-management.service';
import {StudentModel} from '../../../config/http/response-data-models/student.model';
import {StudentCreateModel} from '../../../config/form-configs/students-management/student-create.model';
import {Router} from '@angular/router';
import {InformationBarService} from '../../../services/informational-bar/information-bar.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent {
  @ViewChild(FormBuilderComponent) form: FormBuilderComponent;

  regConfig: IFieldConfig[] = StudentCreateModel;

  public isLoading = false;

  constructor(private studentsManagementService: StudentsManagementService, private router: Router, private informationBarService: InformationBarService) { }


  submit(value: any) {
    this.isLoading = true;

    this.studentsManagementService.CreateStudent(value)
      .subscribe((student: StudentModel) => this.studentCreated(student));
  }

  studentCreated(student: StudentModel): void {
    this.isLoading = false;
    this.informationBarService.showInformation(`Student: ${student.firstName} successfully created!`);
    this.router.navigate([`back-office/students/edit/${student.id}`]);
  }
}
