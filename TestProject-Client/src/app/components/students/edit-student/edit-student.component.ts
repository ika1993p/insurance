import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilderComponent} from '../../../shared/dynamic-form/form-builder/form-builder.component';
import {IFieldConfig} from '../../../shared/dynamic-form/form-builder/IFieldConfig';
import {ActivatedRoute, Router} from '@angular/router';
import {InformationBarService} from '../../../services/informational-bar/information-bar.service';
import {PermissionsService} from '../../../services/http/services/account/permissions/permissions.service';
import {AccountService} from '../../../services/http/services/account/account.service';
import {UtilHelper} from '../../../helpers/UtilHelper';
import {PermissionsConfig} from '../../../services/http/services/account/permissions/permissions.config';
import {EditUserManagementFieldsConfig} from '../../../config/form-configs/users-management/edit-user-management-field.config';
import {EditUserManagementConfig} from '../../../config/form-configs/users-management/edit-user-management.config';
import {StudentsManagementService} from '../../../services/http/services/students/students-management.service';
import {StudentModel} from '../../../config/http/response-data-models/student.model';
import {StudentCreateModel} from '../../../config/form-configs/students-management/student-create.model';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.scss']
})
export class EditStudentComponent implements OnInit {

  @ViewChild(FormBuilderComponent) form: FormBuilderComponent;

  public regConfig: IFieldConfig[] = [];

  public isLoading = false;

  private studentId: number;

  public student: StudentModel = {} as StudentModel;

  public canDeleteStudent;

  public constructor(private activatedRoute: ActivatedRoute,
                     private informationBarService: InformationBarService,
                     private permissionService: PermissionsService,
                     private router: Router,
                     public studentsManagementService: StudentsManagementService,
                     private accountService: AccountService) {
  }

  public ngOnInit(): void {
    this.isLoading = true;
    this.studentId = parseInt(this.activatedRoute.snapshot.params.studentId, 10);

    this.studentsManagementService.getStudent(this.studentId)
      .subscribe((data: StudentModel): void => this.initializeQuoteData(data));
  }

  private initializeQuoteData(data: StudentModel) {
    this.student = data;

    this.canDeleteStudent = this.permissionService.hasPermission(this.accountService.getUser().permission, PermissionsConfig.DeleteUsers);

    const fieldConfigs: IFieldConfig[] = UtilHelper.deepCopy(StudentCreateModel);

    this.regConfig = UtilHelper.updateValuesInFieldsConfig(fieldConfigs, data);
    this.isLoading = false;
  }

  public submit(value): void {
    this.isLoading = true;
    this.studentsManagementService.UpdateStudent({
      firstName: value.firstName,
      lastName: value.lastName,
      dateOfBirth: value.dateOfBirth,
      personalNumber: value.personalNumber,
      sex: value.sex,
      id: this.studentId,
    } as StudentModel)
      .subscribe((data: StudentModel) => this.userUpdated(data));
  }

  public userUpdated(student: StudentModel): void {
    this.isLoading = false;
    this.informationBarService.showInformation(`Student: ${student.firstName} successfully updated!`);
  }

  public studentWasDeleted(): void {
    this.isLoading = false;
    this.informationBarService.showInformation(`Student: ${this.student.firstName} successfully deleted!`);

    this.router.navigate(['back-office/students']);
  }

  public deleteStudent(): void {
    if (!this.canDeleteStudent) {
      return;
    }

    this.isLoading = true;

    this.studentsManagementService.deleteStudent(this.student.id)
      .subscribe(() =>  this.studentWasDeleted());
  }

}
