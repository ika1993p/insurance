import { Component, OnInit } from '@angular/core';
import {UsersTableColumnConfig} from '../../../config/table-config/users-table-column.config';
import {Router} from '@angular/router';
import {AccountService} from '../../../services/http/services/account/account.service';
import {PermissionsService} from '../../../services/http/services/account/permissions/permissions.service';
import {PermissionsConfig} from '../../../services/http/services/account/permissions/permissions.config';
import {UserModel} from '../../../config/http/response-data-models/user.model';
import {StudentsManagementService} from '../../../services/http/services/students/students-management.service';
import {StudentsTableConfig} from '../../../config/table-config/students-table.config';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent {

  public tableColumnConfig = StudentsTableConfig;

  public canEdit;

  public constructor(public studentsManagementService: StudentsManagementService,
                     private router: Router,
                     private accountService: AccountService,
                     private permissionService: PermissionsService) {
    this.canEdit = this.permissionService.hasPermission(this.accountService.getUser().permission, PermissionsConfig.EditUsers);
  }

  public onRowChoose(rowData: UserModel): void {
    if (!this.canEdit) {
      return;
    }

    this.router.navigate([`back-office/students/edit/${rowData.id}`]);
  }

}
