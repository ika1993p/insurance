import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilderComponent } from 'src/app/shared/dynamic-form/form-builder/form-builder.component';
import { IFieldConfig } from 'src/app/shared/dynamic-form/form-builder/IFieldConfig';
import {UsersManagementService} from '../../../services/http/services/users-management/users-management.service';
import {SignupFieldsConfig} from '../../../config/form-configs/account/signup-fields.config';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent {
  @ViewChild(FormBuilderComponent) form: FormBuilderComponent;

  regConfig: IFieldConfig[] = SignupFieldsConfig;

  public isLoading = false;

  constructor(private usersManagementService: UsersManagementService) { }
  submit(value: any) {
    this.isLoading = true;
    this.usersManagementService.CreateUser(value);
  }
}
