import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilderComponent } from 'src/app/shared/dynamic-form/form-builder/form-builder.component';
import { IFieldConfig } from 'src/app/shared/dynamic-form/form-builder/IFieldConfig';
import {UsersManagementService} from '../../../services/http/services/users-management/users-management.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilHelper} from '../../../helpers/UtilHelper';
import {EditUserManagementFieldsConfig} from '../../../config/form-configs/users-management/edit-user-management-field.config';
import {UserEditingModel} from '../../../config/http/response-data-models/user-editting.model';
import {PermissionsService} from '../../../services/http/services/account/permissions/permissions.service';
import {PermissionsConfig} from '../../../services/http/services/account/permissions/permissions.config';
import {EnumObjectConfig} from '../../../config/enum-object.config';
import {EditUserManagementConfig} from '../../../config/form-configs/users-management/edit-user-management.config';
import {UserCreateModel} from '../../../config/http/request-data-models/user-create.model';
import {InformationBarService} from '../../../services/informational-bar/information-bar.service';
import {AccountService} from '../../../services/http/services/account/account.service';
import {EditUserPermissionsFieldConfig} from '../../../config/form-configs/users-management/edit-user-permissions-field.config';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  @ViewChild(FormBuilderComponent) form: FormBuilderComponent;

  public regConfig: IFieldConfig[] = [];

  public isLoading = false;

  private userId: number;

  public user: UserEditingModel = {} as UserEditingModel;

  public canDeleteUser;

  public isUserEditingItSelf = false;

  public constructor(private activatedRoute: ActivatedRoute,
                     private informationBarService: InformationBarService,
                     private permissionService: PermissionsService,
                     private router: Router,
                     public usersManagementService: UsersManagementService,
                     private accountService: AccountService) {
  }

  public ngOnInit(): void {
    this.isLoading = true;
    this.userId = parseInt(this.activatedRoute.snapshot.params.userId, 10);

    this.usersManagementService.getUser(this.userId)
      .subscribe((data: UserEditingModel): void => this.initializeQuoteData(data));
  }

  private initializeQuoteData(data: UserEditingModel) {
    this.user = data;

    const userExistingPermissions: EnumObjectConfig[] = this.permissionService
      .getPermissions(this.user.permission, UtilHelper.enumToObject(PermissionsConfig));

    this.isUserEditingItSelf = this.userId === this.accountService.getUser().id;

    this.canDeleteUser = !this.isUserEditingItSelf && this.permissionService
      .hasPermission(this.accountService.getUser().permission, PermissionsConfig.DeleteUsers);

    let fieldConfigs: IFieldConfig[] = UtilHelper.deepCopy(EditUserManagementFieldsConfig);

    if (!this.isUserEditingItSelf) {
      const lastElement = fieldConfigs.pop();
      fieldConfigs = fieldConfigs.concat(EditUserPermissionsFieldConfig);
      fieldConfigs.push(lastElement);
    }

    let updatedData = UtilHelper.updateValuesInFieldsConfig(fieldConfigs, data);
    updatedData = UtilHelper.updatePermissionValuesInFieldsConfig(updatedData, userExistingPermissions);

    this.regConfig = updatedData;
    this.isLoading = false;
  }

  public submit(value: EditUserManagementConfig): void {
    this.isLoading = true;
    const newUserPermission = this.permissionService.createUserPermission(value);

    this.usersManagementService.UpdateUser({
      permission: newUserPermission,
      id: this.user.id,
      email: value.email,
      userName: value.userName,
      password: value.password
    } as UserCreateModel)
      .subscribe((data: UserEditingModel) => this.userUpdated(data));
  }

  public userUpdated(user: UserEditingModel): void {
    this.isLoading = false;
    this.informationBarService.showInformation(`User: ${user.userName} successfully updated!`);
  }

  public userWasDeleted(): void {
    this.isLoading = false;
    this.informationBarService.showInformation(`User: ${this.user.userName} successfully deleted!`);

    this.router.navigate(['back-office/users']);
  }

  public deleteUser(): void {
    if (!this.canDeleteUser) {
      return;
    }

    this.isLoading = true;

    this.usersManagementService.deleteUser(this.user.id)
      .subscribe(() =>  this.userWasDeleted());
  }
}
