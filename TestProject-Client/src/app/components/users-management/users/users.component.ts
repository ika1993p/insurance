import {Component} from '@angular/core';
import {UsersTableColumnConfig} from '../../../config/table-config/users-table-column.config';
import {UsersManagementService} from '../../../services/http/services/users-management/users-management.service';
import {UserModel} from '../../../config/http/response-data-models/user.model';
import {Router} from '@angular/router';
import {AccountService} from '../../../services/http/services/account/account.service';
import {PermissionsService} from '../../../services/http/services/account/permissions/permissions.service';
import {PermissionsConfig} from '../../../services/http/services/account/permissions/permissions.config';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  public tableColumnConfig = UsersTableColumnConfig;

  public canEdit;

  public constructor(public usersManagementService: UsersManagementService,
                     private router: Router,
                     private accountService: AccountService,
                     private permissionService: PermissionsService) {
    this.canEdit = this.permissionService.hasPermission(this.accountService.getUser().permission, PermissionsConfig.EditUsers);
  }

  public onRowChoose(rowData: UserModel): void {
    if (!this.canEdit) {
      return;
    }

    this.router.navigate([`back-office/users/edit/${rowData.id}`]);
  }
}
