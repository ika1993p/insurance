export interface EnumObjectConfig{
  readonly value: number;
  readonly name: string;
}
