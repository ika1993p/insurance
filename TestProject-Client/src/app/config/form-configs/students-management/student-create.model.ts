import {IFieldConfig} from '../../../shared/dynamic-form/form-builder/IFieldConfig';
import {Validators} from '@angular/forms';
import {ageValidator} from '../../../services/validators/age.validator';

export const StudentCreateModel: IFieldConfig[] = [
  {
    type: 'input',
    label: 'Personal Number',
    inputType: 'number',
    name: 'personalNumber',
    validations: [
      {
        name: 'required',
        validator: Validators.required,
        message: 'Id Required'
      },
      {
        name: 'pattern',
        validator: Validators.pattern('^(\\d{11})$'),
        message: 'Invalid id. Id must contains only number and there must be eleven digits'
      }
    ]
  },
  {
    type: 'input',
    label: 'First Name',
    inputType: 'text',
    name: 'firstName',
    validations: [
      {
        name: 'required',
        validator: Validators.required,
        message: 'First name Required'
      },
      {
        name: 'pattern',
        validator: Validators.pattern('[a-zA-Z]{2,32}'),
        message: 'Incorrect first name. only letters. 2-32 characters'
      },
    ]
  },
  {
    type: 'input',
    label: 'Last Name',
    inputType: 'text',
    name: 'lastName',
    validations: [
      {
        name: 'required',
        validator: Validators.required,
        message: 'Last name Required'
      },
      {
        name: 'pattern',
        validator: Validators.pattern('[a-zA-Z]{5,32}'),
        message: 'Incorrect last name. only letters. 5-32 characters'
      },
    ]
  },
  {
    type: 'date',
    label: 'Date of birth',
    name: 'dateOfBirth',
    validations: [
      {
        name: 'required',
        validator: Validators.required,
        message: 'Date of Birth Required'
      },
      {
        name: 'ageValidator',
        validator: ageValidator,
        message: 'Student age could not be les than 16'
      }
    ]
  },
  {
    type: 'radiobutton',
    label: 'Gender',
    name: 'sex',
    options: ['Male', 'Female'],
    value: 'Male'
  },
  {
    type: 'button',
    label: 'Save'
  }
];
