import { IFieldConfig } from 'src/app/shared/dynamic-form/form-builder/IFieldConfig';
import { Validators } from '@angular/forms';

export const EditUserManagementFieldsConfig: IFieldConfig[] = [
  {
    type: 'input',
    label: 'Username',
    inputType: 'text',
    name: 'userName',
    validations: [
      {
        name: 'required',
        validator: Validators.required,
        message: 'Username Required'
      },
      {
        name: 'minlength',
        validator: Validators.minLength(3),
        message: 'Username must contain minimum 3 characters'
      }
      ,
      {
        name: 'maxlength',
        validator: Validators.maxLength(32),
        message: 'Username must contain maximum 32 characters'
      }
    ]
  },
  {
    type: 'input',
    label: 'Email Address',
    inputType: 'email',
    name: 'email',
    validations: [
      {
        name: 'required',
        validator: Validators.required,
        message: 'Email Required'
      },
      {
        name: 'maxlength',
        validator: Validators.maxLength(100),
        message: 'Email must contain maximum 100 characters'
      },
      {
        name: 'pattern',
        validator: Validators.pattern(
          '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'
        ),
        message: 'Invalid email'
      }
    ]
  },
  {
    type: 'input',
    label: 'Password',
    inputType: 'password',
    name: 'password',
    validations: [
      {
        name: 'minlength',
        validator: Validators.minLength(8),
        message: 'Password must contain minimum 8 characters'
      },
      {
        name: 'maxlength',
        validator: Validators.maxLength(32),
        message: 'Password must contain maximum 32 characters'
      }
    ]
  },
  {
    type: 'button',
    label: 'Save'
  }
];
