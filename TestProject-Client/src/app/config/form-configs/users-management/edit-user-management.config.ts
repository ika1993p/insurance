import {UserEditingModel} from '../../http/response-data-models/user-editting.model';

export interface EditUserManagementConfig extends UserEditingModel {
  readonly password: string;
  readonly viewUsers: boolean;
  readonly editUsers: boolean;
  readonly addUsers: boolean;
  readonly deleteUsers: boolean;
  readonly viewQuotes: boolean;
  readonly editQuotes: boolean;
  readonly addQuotes: boolean;
  readonly deleteQuotes: boolean;
  readonly reviewQuize: boolean;
}
