import {IFieldConfig} from '../../../shared/dynamic-form/form-builder/IFieldConfig';

export const EditUserPermissionsFieldConfig: IFieldConfig[] = [{
    type: 'checkbox',
    label: 'View users',
    name: 'ViewUsers',
    value: false
  },
  {
    type: 'checkbox',
    label: 'Edit users',
    name: 'EditUsers',
    value: false
  },
  {
    type: 'checkbox',
    label: 'Add users',
    name: 'AddUsers',
    value: false
  },
  {
    type: 'checkbox',
    label: 'Delete users',
    name: 'DeleteUsers',
    value: false
  },
  {
    type: 'checkbox',
    label: 'View students',
    name: 'ViewStudents',
    value: false
  },
  {
    type: 'checkbox',
    label: 'Edit students',
    name: 'EditStudents',
    value: false
  },
  {
    type: 'checkbox',
    label: 'Add students',
    name: 'AddStudents',
    value: false
  },
  {
    type: 'checkbox',
    label: 'Delete students',
    name: 'DeleteStudents',
    value: false
  },
];

