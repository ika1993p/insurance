export class ApiEndpoints {
  // Base API url
  private static readonly API: string = 'https://localhost:44306/api';

  // Account Controller
  public static readonly SignIn: string = `${ApiEndpoints.API}/account/signin`;
  public static readonly SignUp: string = `${ApiEndpoints.API}/account/signup`;

  // User Controller
  public static readonly GetUsers: string = `${ApiEndpoints.API}/User`;
  public static readonly UpdateUser: string = `${ApiEndpoints.API}/User`;
  public static readonly CreateUser: string = `${ApiEndpoints.API}/User`;
  public static readonly GetUser: string = `${ApiEndpoints.API}/User/$1`;
  public static readonly DeleteUser: string = `${ApiEndpoints.API}/User/$1`;

  // User Controller
  public static readonly GetStudents: string = `${ApiEndpoints.API}/Students`;
  public static readonly UpdateStudent: string = `${ApiEndpoints.API}/Students`;
  public static readonly CreateStudent: string = `${ApiEndpoints.API}/Students`;
  public static readonly GetStudent: string = `${ApiEndpoints.API}/Students/$1`;
  public static readonly DeleteStudent: string = `${ApiEndpoints.API}/Students/$1`;
}
