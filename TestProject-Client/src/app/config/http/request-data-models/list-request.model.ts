export interface ListRequestModel {
  readonly start: number;
  readonly limit: number;
  readonly sort: string[];
}
