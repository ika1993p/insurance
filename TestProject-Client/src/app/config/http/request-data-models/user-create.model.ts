import {UserEditingModel} from '../response-data-models/user-editting.model';

export interface UserCreateModel extends UserEditingModel {
  readonly password: string;
}
