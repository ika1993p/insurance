export interface ListViewModel<T> {
  readonly totalRecords: number;
  readonly data: T[];
}
