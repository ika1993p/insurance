export interface StudentModel {
  readonly id: number;
  readonly personalNumber: number;
  readonly firstName: string;
  readonly lastName: string;
  readonly dateOfBirth;
  readonly sex: string;
}
