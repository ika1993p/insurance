import {UserModel} from './user.model';

export interface UserEditingModel extends UserModel {
  readonly permission: number;
}
