export interface UserSigninModel {
  readonly id: number;
  readonly userName: string;
  readonly jwtToken: string;
  readonly permission: number;
}
