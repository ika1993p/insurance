export interface UserModel {
  readonly id: number;
  readonly userName: string;
  readonly email: string;
}
