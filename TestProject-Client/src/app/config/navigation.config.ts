import {PermissionsConfig} from '../services/http/services/account/permissions/permissions.config';


export const NavigationConfig = {
  quotes: {
    name: 'Students',
    permission: PermissionsConfig.ViewStudents,
    url: '/back-office/students'
  },
  users: {
    name: 'Users',
    permission: PermissionsConfig.ViewUsers,
    url: '/back-office/users'
  }
};
