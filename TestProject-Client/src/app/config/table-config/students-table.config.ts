import {TableConfig} from './table.config';

export const StudentsTableConfig: TableConfig[] = [
  {
    value: 'Student Id',
    name: 'id'
  },
  {
    value: 'Personal Number',
    name: 'personalNumber'
  },
  {
    value: 'First Name',
    name: 'firstName'
  },
  {
    value: 'Last Name',
    name: 'lastName'
  },
  {
    value: 'Date Of birth',
    name: 'dateOfBirth'
  },
  {
    value: 'Gender',
    name: 'sex'
  }
];
