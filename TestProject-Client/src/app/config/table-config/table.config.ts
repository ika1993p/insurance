export interface TableConfig {
  readonly value: string;
  readonly name: string;
}
