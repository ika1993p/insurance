import {TableConfig} from './table.config';

export const UsersTableColumnConfig: TableConfig[] = [
  {
    name: 'id',
    value: 'User Id'
  },
  {
    name: 'userName',
    value: 'Username'
  },
  {
    name: 'email',
    value: 'User Email'
  }
];
