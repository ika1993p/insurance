import {IFieldConfig} from '../shared/dynamic-form/form-builder/IFieldConfig';
import {EnumObjectConfig} from '../config/enum-object.config';

export class UtilHelper {
  static updateValuesInFieldsConfig(model: IFieldConfig[], values: any): IFieldConfig[] {
    model = UtilHelper.deepCopy(model);
    Object.keys(values).forEach((valueItem) => {
      model.forEach((modelItem) => {
        if (modelItem.name === valueItem) {
          modelItem.value = values[valueItem];
        }
      });
    });

    return model;
  }

  static updatePermissionValuesInFieldsConfig(model: IFieldConfig[], values: EnumObjectConfig[]): IFieldConfig[] {
    model = UtilHelper.deepCopy(model);
    Object.keys(values).forEach((valueItem) => {
      model.forEach((modelItem) => {
        if (modelItem.name === values[valueItem].name) {
          modelItem.value = true;
        }
      });
    });

    return model;
  }

  static replaceString(str: string, params: (string | number)[]): string {
    for (let i = 0; i < params.length ; i++) {
      const st = '$' + (i + 1);
      const param = params[i].toString();
      str = str.split(st)
        .join(param);
    }

    return str;
  }

  static enumToObject(enumObject: any): EnumObjectConfig[] {
    const map: EnumObjectConfig[] = [];

    for (const n in enumObject) {
      if (typeof enumObject[n] === 'number') {
        map.push({value: enumObject[n] as any, name: n});
      }
    }

    return map;
  }

  static deepCopy(aObject) {
    if (!aObject) {
      return aObject;
    }

    let v;
    const bObject = Array.isArray(aObject) ? [] : {};
    // tslint:disable-next-line:forin
    for (const k in aObject) {
      v = aObject[k];
      bObject[k] = (typeof v === 'object') ? UtilHelper.deepCopy(v) : v;
    }

    return bObject;
  }
}
