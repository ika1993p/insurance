import { TestBed } from '@angular/core/testing';
import {SignInGuardService} from './account.guard';

describe('AuthGuardServiceService', () => {
  let service: SignInGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SignInGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
