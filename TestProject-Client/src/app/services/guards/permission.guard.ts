import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs';
import { AccountService } from '../http/services/account/account.service';
import { PermissionsService } from '../http/services/account/permissions/permissions.service';
import {UserSigninModel} from '../../config/http/response-data-models/user-signin.model';
import has = Reflect.has;

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {
  public constructor(public accountService: AccountService,
                     public permissionsService: PermissionsService,
                     public router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const user: UserSigninModel = this.accountService.getUser();
    const hasPermission = this.permissionsService.hasPermission(user.permission, next.data.mandatoryPermission);

    return hasPermission;
  }
}
