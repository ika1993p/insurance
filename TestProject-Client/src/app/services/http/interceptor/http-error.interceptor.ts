import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {InformationBarService} from '../../informational-bar/information-bar.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private informationBarService: InformationBarService) {}


  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          const statusCode: number = error.status;
          const statusText: string = error.statusText;
          this.informationBarService.showInformation('Error');
          return throwError(statusCode);
        })
      );
  }
}
