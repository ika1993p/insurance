import { TestBed } from '@angular/core/testing';

import { JwtAuthorizationInterceptor } from './jwt-authorization.interceptor';

describe('JwtAuthorizationInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      JwtAuthorizationInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: JwtAuthorizationInterceptor = TestBed.inject(JwtAuthorizationInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
