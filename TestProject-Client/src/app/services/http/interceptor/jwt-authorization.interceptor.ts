import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {LocalStorageService} from '../../local-storage/local-storage.service';
import {UserSigninModel} from '../../../config/http/response-data-models/user-signin.model';

@Injectable()
export class JwtAuthorizationInterceptor implements HttpInterceptor {
  constructor(private localStorage: LocalStorageService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const user: UserSigninModel = JSON.parse(this.localStorage.get('user'));

    if (user && user.jwtToken) {
      request = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + user.jwtToken)
      });
    }

    return next.handle(request);
  }
}
