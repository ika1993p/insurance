import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {LocalStorageService} from '../../../local-storage/local-storage.service';
import {HttpClient} from '@angular/common/http';
import {Observable, Subscription} from 'rxjs';
import {ApiEndpoints} from '../../../../config/http/api-endpoints';
import {Router} from '@angular/router';
import {UserSigninModel} from '../../../../config/http/response-data-models/user-signin.model';
import {SignInModel} from '../../../../config/http/request-data-models/signIn.model';
import {UserCreateModel} from '../../../../config/http/request-data-models/user-create.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  public constructor(private jwtHelper: JwtHelperService,
                     private localStorage: LocalStorageService,
                     private http: HttpClient,
                     private router: Router) {}

  public isAuthenticated(): boolean {
    const user = this.getUser();

    if (user && user.jwtToken) {
      try {
        return !this.isTokenExpired(user.jwtToken);
      } catch (e) {
        this.localStorage.remove('user');
        return false;
      }
    }
    return false;
  }

  private isTokenExpired(token: string): boolean {
    if (this.jwtHelper.isTokenExpired(token)) {
      this.localStorage.remove('user');
      return true;
    }

    return false;
  }

  public signUp(user: UserCreateModel): Observable<void> {
    return this.http.post<void>(ApiEndpoints.SignUp, user);
  }

  public signIn(user: SignInModel): Observable<UserSigninModel> {
    return this.http.post<UserSigninModel>(ApiEndpoints.SignIn, user);
  }

  public getUser(): UserSigninModel {
    return JSON.parse(this.localStorage.get('user'));
  }

  public logOut(): void {
    this.localStorage.remove('user');
    this.router.navigate(['signin']);
  }
}
