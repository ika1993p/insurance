import { Injectable } from '@angular/core';
import {EnumObjectConfig} from '../../../../../config/enum-object.config';
import {EditUserManagementConfig} from '../../../../../config/form-configs/users-management/edit-user-management.config';
import {PermissionsConfig} from './permissions.config';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {
  public hasPermission(userPermission: number, mandatoryPermission: PermissionsConfig): boolean {
    if (mandatoryPermission === 0) {
      return true;
    }

    return (userPermission & mandatoryPermission) !== 0;
  }

  public getPermissions(userPermission: number, permissions: EnumObjectConfig[]) {
    const userPermissions: EnumObjectConfig[] = [];

    permissions.forEach((permission) => {
      if ((userPermission & permission.value) === permission.value) {
        userPermissions.push(permission);
      }
    });

    return userPermissions;
  }

  public createUserPermission(userEditingData: EditUserManagementConfig): number {
    let permission = 0;

    Object.keys(userEditingData).forEach((key: string) => {
      if (PermissionsConfig[key] && userEditingData[key]) {
        permission += PermissionsConfig[key];
      }
    });
    return  permission;
  }
}
