import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiEndpoints} from '../../../../config/http/api-endpoints';
import {ListViewModel} from '../../../../config/http/response-data-models/list-view.model';
import {UtilHelper} from '../../../../helpers/UtilHelper';
import {StudentModel} from '../../../../config/http/response-data-models/student.model';

@Injectable({
  providedIn: 'root'
})
export class StudentsManagementService {

  public constructor(private http: HttpClient) {}

  public getStudents(queryParams: HttpParams): Observable<ListViewModel<StudentModel>> {
    return this.http.get<ListViewModel<StudentModel>>(ApiEndpoints.GetStudents, {
      params: queryParams
    });
  }

  public getStudent(studentId: number): Observable<StudentModel> {
    return this.http.get<StudentModel>(
      UtilHelper.replaceString(ApiEndpoints.GetStudent, [studentId])
    );
  }

  public UpdateStudent(student: StudentModel): Observable<StudentModel> {
    return this.http.post<StudentModel>(ApiEndpoints.UpdateStudent, student);
  }

  public CreateStudent(student: any): Observable<StudentModel> {
    return this.http.put<any>(ApiEndpoints.CreateStudent, student);
  }

  public deleteStudent(studentId: number): Observable<void> {
    return this.http.delete<void>(
      UtilHelper.replaceString(ApiEndpoints.DeleteStudent, [studentId])
    );
  }
}
