import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {ApiEndpoints} from '../../../../config/http/api-endpoints';
import {InformationBarService} from '../../../informational-bar/information-bar.service';
import {ListViewModel} from '../../../../config/http/response-data-models/list-view.model';
import {UserModel} from '../../../../config/http/response-data-models/user.model';
import {UtilHelper} from '../../../../helpers/UtilHelper';
import {UserCreateModel} from '../../../../config/http/request-data-models/user-create.model';

@Injectable({
  providedIn: 'root'
})
export class UsersManagementService {
  public constructor(private http: HttpClient,
                     private router: Router,
                     private informationBarService: InformationBarService) {}

  public getUsers(queryParams: HttpParams): Observable<ListViewModel<UserModel>> {
    return this.http.get<ListViewModel<UserModel>>(ApiEndpoints.GetUsers, {
      params: queryParams
    });
  }

  public getUser(userId: number): Observable<UserModel> {
    return this.http.get<UserModel>(
      UtilHelper.replaceString(ApiEndpoints.GetUser, [userId])
    );
  }

  public UpdateUser(user: UserCreateModel): Observable<UserModel> {
    return this.http.post<UserModel>(ApiEndpoints.UpdateUser, user);
  }

  public CreateUser(user: any): Subscription {
    return this.http.put<any>(ApiEndpoints.CreateUser, user, )
      .subscribe((data: any) => {
        this.informationBarService.showInformation('User successfully created');
        this.router.navigate([`back-office/users/edit/${data.id}`]);
      });
  }

  public deleteUser(userId: number): Observable<void> {
    return this.http.delete<void>(
        UtilHelper.replaceString(ApiEndpoints.DeleteUser, [userId])
      );
  }
}
