import { TestBed } from '@angular/core/testing';

import { InformationBarService } from './information-bar.service';

describe('InformationBarService', () => {
  let service: InformationBarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InformationBarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
