import {Injectable, Injector} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class InformationBarService {
  private readonly snackBar: MatSnackBar;

  constructor(private injector: Injector) {
    this.snackBar = this.injector.get(MatSnackBar);
  }

  showInformation(infoText: string, duration: number = 2000) {
    this.snackBar.open(infoText, '', {
      duration
    });
  }
}
