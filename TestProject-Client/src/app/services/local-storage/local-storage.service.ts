import { Injectable } from '@angular/core';
import {StorageItem} from './storage-item';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  localStorageSupported: boolean;

  public constructor() {
    this.localStorageSupported = typeof window.localStorage !== 'undefined' && window.localStorage != null;
  }

  public add(key: string, item: string) {
    if (this.localStorageSupported) {
      localStorage.setItem(key, item);
    }
  }

  public getAllItems(): Array<StorageItem> {
    const list = new Array<StorageItem>();

    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);

      list.push(new StorageItem({
        key,
        value
      }));
    }

    return list;
  }

  public getAllValues(): Array<any> {
    const list = new Array<any>();

    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);

      list.push(value);
    }

    return list;
  }

  public get(key: string): string {
    if (this.localStorageSupported) {
      const item = localStorage.getItem(key);
      return item;
    } else {
      return null;
    }
  }

  public remove(key: string) {
    if (this.localStorageSupported) {
      localStorage.removeItem(key);
    }
  }

  public clear() {
    if (this.localStorageSupported) {
      localStorage.clear();
    }
  }
}
