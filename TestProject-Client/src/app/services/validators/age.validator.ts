import {AbstractControl} from '@angular/forms';

export function ageValidator(control: AbstractControl) {
  // @ts-ignore
  const ageDifMs = Date.now() - new Date(control.value);
  const ageDate = new Date(ageDifMs);
  const age = Math.abs(ageDate.getUTCFullYear() - 1970);

  if (age < 16) {
    return {
      ageValidator: {
        ageValidator: false
      }
    };
  }

  return null;
}
