import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {ListViewModel} from '../../config/http/response-data-models/list-view.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {merge} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {ListRequestModel} from '../../config/http/request-data-models/list-request.model';
import {TableConfig} from '../../config/table-config/table.config';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-data-table',
  styleUrls: ['data-table.component.scss'],
  templateUrl: 'data-table.component.html',
})
export class DataTableComponent<T> implements AfterViewInit, OnInit {
  @Input() columns: TableConfig[] = [];

  @Input() canEdit = false;

  columnNames: string[] = [];

  @Input() dataRetriever: (params: ListRequestModel) => ListViewModel<T>;

  @Input() onRowChoose: (rowData: T) => void;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  public defaultActiveSortColumn: any;

  public tableData: ListViewModel<T> = {
    data: [],
    totalRecords: 0
  };

  public isLoadingResults = true;

  ngOnInit(): void {
    this.defaultActiveSortColumn = this.columns[0].name;
    this.columnNames = this.columns.map((obj) => obj.name);
  }

  public ngAfterViewInit() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          const params: ListRequestModel = {
            limit: this.paginator.pageSize,
            sort: [this.sort.active, this.sort.direction],
            start: this.paginator.pageIndex + 1
          } as ListRequestModel;

          this.isLoadingResults = true;
          return this.dataRetriever.call(this, params);
        }),
        map((result: ListViewModel<T>) => {
          this.isLoadingResults = false;

          return result;
        })
      ).subscribe((data: ListViewModel<T>) => { this.tableData = data; });
  }
}
