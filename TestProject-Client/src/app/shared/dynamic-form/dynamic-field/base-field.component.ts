import {IFieldConfig} from '../form-builder/IFieldConfig';
import {FormGroup} from '@angular/forms';

export class BaseFieldComponent {
  field: IFieldConfig;
  group: FormGroup;
}
