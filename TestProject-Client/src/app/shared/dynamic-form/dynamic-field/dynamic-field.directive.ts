import { ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef } from '@angular/core';
import { InputComponent } from '../elements/input/input.component';
import { ButtonComponent } from '../elements/button/button.component';
import { RadioButtonComponent } from '../elements/radio-button/radio-button.component';
import { CheckboxComponent } from '../elements/checkbox/checkbox.component';
import { IFieldConfig } from '../form-builder/IFieldConfig';
import { FormGroup } from '@angular/forms';
import {SelectComponent} from '../elements/select/select.component';
import {DateComponent} from '../elements/date/date.component';

const componentMapper = {
    input: InputComponent,
    button: ButtonComponent,
    radiobutton: RadioButtonComponent,
    checkbox: CheckboxComponent,
    select: SelectComponent,
    date: DateComponent
};
@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[dynamicField]'
})
export class DynamicFieldDirective implements OnInit {
    @Input() field: IFieldConfig;
    @Input() group: FormGroup;
    componentRef: any;
    constructor(private resolver: ComponentFactoryResolver, private container: ViewContainerRef ) {}
    ngOnInit() {
      const factory = this.resolver.resolveComponentFactory(
        componentMapper[this.field.type]
      );
      this.componentRef = this.container.createComponent(factory);
      this.componentRef.instance.field = this.field;
      this.componentRef.instance.group = this.group;
    }
}
