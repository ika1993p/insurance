import { Component, OnInit } from '@angular/core';
import { IFieldConfig } from '../../form-builder/IFieldConfig';
import { FormGroup } from '@angular/forms';
import {BaseFieldComponent} from '../../dynamic-field/base-field.component';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent extends BaseFieldComponent {}
