import { Component, OnInit } from '@angular/core';
import { IFieldConfig } from '../../form-builder/IFieldConfig';
import { FormGroup } from '@angular/forms';
import {BaseFieldComponent} from '../../dynamic-field/base-field.component';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent extends BaseFieldComponent {}
