import { Component, OnInit } from '@angular/core';
import {BaseFieldComponent} from '../../dynamic-field/base-field.component';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent extends BaseFieldComponent {}
