import { Component, OnInit } from '@angular/core';
import { IFieldConfig } from '../../form-builder/IFieldConfig';
import { FormGroup } from '@angular/forms';
import {BaseFieldComponent} from '../../dynamic-field/base-field.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent extends BaseFieldComponent {}
