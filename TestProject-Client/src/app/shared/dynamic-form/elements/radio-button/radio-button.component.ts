import { Component, OnInit } from '@angular/core';
import {BaseFieldComponent} from '../../dynamic-field/base-field.component';

@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss']
})
export class RadioButtonComponent  extends BaseFieldComponent {}
