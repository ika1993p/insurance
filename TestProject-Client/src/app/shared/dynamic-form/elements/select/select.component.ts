import { Component, OnInit } from '@angular/core';
import {BaseFieldComponent} from '../../dynamic-field/base-field.component';


@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent extends BaseFieldComponent {}
