export interface IValidator {
    name: string;
    validator: any;
    message: string;
}

export interface IFieldConfig {
    label?: string;
    name?: string;
    inputType?: string;
    options?: any[];
    collections?: any;
    disabled?: boolean;
    type: string;
    value?: any;
    validations?: IValidator[];
    group?: string;
}
