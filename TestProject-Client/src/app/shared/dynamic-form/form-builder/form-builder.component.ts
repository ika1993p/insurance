import {Component, Output, OnInit, Input, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IFieldConfig } from './IFieldConfig';

@Component({
  selector: 'app-form-builder',
  exportAs: 'dynamicForm',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss']
})
export class FormBuilderComponent implements OnInit, OnChanges {
  @Input() fields: IFieldConfig[] = [];
  @Output() submit: EventEmitter<any> = new EventEmitter<any>();

  form: FormGroup;

  public constructor(private fb: FormBuilder) {}

  public ngOnInit(): void {
    this.form = this.createControl();
  }

  public get value(): void {
    return this.form.value;
  }

  public groupValues(values: any): string {
    this.fields.forEach((a) => {
      if (a.group) {
        if (!values[a.group]) {
          values[a.group] = [];
        }
        values[a.group].push(values[a.name]);
        delete values[a.name];
      }
    });
    return values;
  }

  public selectValues(values: any) {
    this.fields.forEach((a) => {
      if (a.type === 'select') {
        a.options.forEach((item, index) => {
          if (values[a.name] === item.value) {
            item.index = index;
            values[a.name] = item;
            return;
          }
        });
      }
    });

    return values;
  }

  public onSubmit(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
    let values = this.selectValues(this.form.getRawValue());

    values = this.groupValues(values);

    if (this.form.valid) {
      this.submit.emit(values);
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  public createControl(): FormGroup {
    const group = this.fb.group({});
    this.fields.forEach(field => {
      if (field.type === 'button') { return; }
      const control = this.fb.control(
          {value: field.value, disabled: field.disabled},
        this.bindValidations(field.validations || [])
      );
      group.addControl(field.name, control);
    });
    return group;
  }

  public bindValidations(validations: any): Validators {
    if (validations.length > 0) {
      const validList = [];
      validations.forEach(valid => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }

  public validateAllFormFields(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (this.form) {
      this.form.reset();
      this.form = this.createControl();
    }
  }
}
