import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../services/http/services/account/account.service';
import {PermissionsService} from '../../services/http/services/account/permissions/permissions.service';
import {NavigationConfig} from '../../config/navigation.config';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  constructor(private accountService: AccountService,
              public permissionsService: PermissionsService) { }

  navigations = [];

  ngOnInit(): void {
    Object.keys(NavigationConfig).forEach((key) => {
      if (this.permissionsService.hasPermission(this.accountService.getUser().permission, NavigationConfig[key].permission)) {
        this.navigations.push(NavigationConfig[key]);
      }
    });
  }

  logOut() {
    this.accountService.logOut();
  }
}
