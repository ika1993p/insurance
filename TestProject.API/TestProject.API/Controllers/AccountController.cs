﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TestProject.Application.Attributes;
using TestProject.Application.DTO.Requests;
using TestProject.Application.DTO.Responses;
using TestProject.Domain.Contracts;
using TestProject.Domain.Entities;
using TestProject.Helpers;
using Microsoft.Extensions.Logging;
using TestProject.Application.DTOs.Responses;
using System.ComponentModel.DataAnnotations;

namespace TestProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ModelValidation]
    public class AccountController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        private readonly IConfiguration _config;

        private readonly ILogger<AccountController> _logger;

        public AccountController(IUserRepository userRepository, IMapper mapper, IConfiguration config, ILogger<AccountController> logger)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _config = config;
            _logger = logger;
        }

        [HttpPost("signin")]
        public ActionResult<UserCredentialsDTO> SinIn([FromBody] SignInCredentialsDTO credentials)
        {
            var user = _userRepository
                .GetByEmail(credentials.Email);

            if (user == null)
                return NotFound();

            var hashedPassword = Encoding.Default.GetString(user.Password);
            
            if (!PasswordHelper.VerifyPassword(hashedPassword, credentials.Password))
            {
                _logger.LogInformation("Invalid password");

                return Forbid();
            }

            var userCredentialsDTO = _mapper.Map<UserCredentialsDTO>(user);

            userCredentialsDTO.JwtToken = GenerateJwt(user);

            return userCredentialsDTO;
        }

        [HttpPost("signup")]
        public ActionResult<UserDTO> SinUp([FromBody] UserCreateDTO userData)
        {
            var userController = new UserController(_userRepository, _mapper);

            return userController.CreateUser(userData);
        }

        [HttpGet("email/{email}/is-unique")]
        public ActionResult<IsUniqueStatusDTO> IsMailUnique([EmailAddress, MaxLength(100)]string email)
        {
            return new IsUniqueStatusDTO 
            {
                IsUnique = _userRepository.IsUserEmailUnique(email)
            };
        }

        [HttpGet("username/{userName}/is-unique")]
        public ActionResult<IsUniqueStatusDTO> IsUsernameUnique([StringLength(100, MinimumLength = 3)]string userName)
        {
            return new IsUniqueStatusDTO
            {
                IsUnique = _userRepository.IsUserNameUnique(userName)
            };
        }

        [NonAction]
        private string GenerateJwt(UserEntity user)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim("Permission", user.Permission.ToString()),
            };

            var expireTime = DateTime.Now.AddMinutes(int.Parse(_config["AppSettings:JWT:LifeTimeMinutes"]));

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_config["AppSettings:JWT:Secret"]));
            var credentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var jwt = new JwtSecurityToken(null, null, claims, null, expireTime, credentials);
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}
