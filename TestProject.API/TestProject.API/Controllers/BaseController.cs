﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace TestProject.API.Controllers
{
    public class BaseController : ControllerBase
    {
        protected int UserId => int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

        protected string UserName => User.FindFirstValue(ClaimTypes.Name);

        protected int Permission => int.Parse(User.FindFirstValue("Permission"));

        protected string Email => User.FindFirstValue(ClaimTypes.Email);
    }
}