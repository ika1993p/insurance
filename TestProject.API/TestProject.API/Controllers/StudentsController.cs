﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestProject.Application.Attributes;
using TestProject.Application.Constants;
using TestProject.Application.DTO.Requests;
using TestProject.Application.DTO.Responses;
using TestProject.Application.DTOs.Requests;
using TestProject.Application.DTOs.Responses;
using TestProject.Domain.Contracts;
using TestProject.Domain.Entities;
using TestProject.Domain.Models;

namespace TestProject.API.Controllers
{
    [Route("api/[controller]")]
    [ModelValidation]
    [Authorize]
    public class StudentsController : BaseController
    {
        private readonly IStudentRepository _studentsRepository;

        private readonly IMapper _mapper;

        private readonly ILogger<StudentsController> _logger;

        public StudentsController(IMapper mapper, IStudentRepository studentsRepository, ILogger<StudentsController> logger)
        {
            _studentsRepository = studentsRepository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        [Permission(UserPermission.ViewStudents)]
        public ActionResult<ListViewDTO<StudentDTO>> GetStudents([FromQuery]ListReuqestDTO<StudentListFilterDTO> queryParameters)
        {
            var students = _studentsRepository.GetAll(queryParameters.Start, queryParameters.Limit, queryParameters.Sort, queryParameters.Filter);

            return new ListViewDTO<StudentDTO>
            {
                Data = _mapper.Map<IEnumerable<StudentDTO>>(students),
                TotalRecords = _studentsRepository.Count(queryParameters.Filter)
            };
        }

        [HttpGet("{studentId}")]
        [Permission(UserPermission.ViewStudents)]
        public ActionResult<StudentDTO> GetStudent(int studentId)
        {
            var userQuizResults = _studentsRepository.GetById(studentId);

            return _mapper.Map<StudentDTO>(userQuizResults);
        }

        [HttpGet("personal-number/{personalNumber}/is-unique")]
        public ActionResult<IsUniqueStatusDTO> IsPersonalNumberUnique([RegularExpression(StudentsValidationConfig.PersonalNumberRegEx)]string personalNumber)
        {
            return new IsUniqueStatusDTO
            {
                IsUnique = _studentsRepository.IsPersonalNumberUnique(personalNumber)
            };
        }

        [HttpDelete("{studentId}")]
        [Permission(UserPermission.DeleteStudents)]
        public IActionResult DeleteStudent(int studentId)
        {
            var user = _studentsRepository.Remove(studentId);

            if (user == null)
                return NotFound();

            return Ok();
        }

        [HttpPost]
        [Permission(UserPermission.EditStudents)]
        public ActionResult<StudentDTO> UpdateStudent([FromBody]StudentModifyDTO studentData)
        {
            var user = _studentsRepository.GetById(studentData.Id);

            if (user == null)
                return NotFound();

            user.PersonalNumber = studentData.PersonalNumber;
            user.DateOfBirth = studentData.DateOfBirth;
            user.FirstName = studentData.FirstName;
            user.LastName = studentData.LastName;
            user.Sex = studentData.Sex;

            _studentsRepository.Update(_mapper.Map<StudentEntity>(user));

            return _mapper.Map<StudentDTO>(user);
        }

        [HttpPut]
        [Permission(UserPermission.EditStudents)]
        public ActionResult<StudentDTO> CreateStudent([FromBody]StudentCreateDTO studentData)
        {
            var newStudent = _mapper.Map<StudentEntity>(studentData);

            _studentsRepository.Add(newStudent);

            return _mapper.Map<StudentDTO>(newStudent);
        }
    }
}
