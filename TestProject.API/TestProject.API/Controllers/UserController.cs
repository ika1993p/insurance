﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Text;
using TestProject.API.Controllers;
using TestProject.Application.Attributes;
using TestProject.Application.DTO.Requests;
using TestProject.Application.DTO.Responses;
using TestProject.Domain.Contracts;
using TestProject.Domain.Entities;
using TestProject.Domain.Models;
using TestProject.Helpers;

namespace TestProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ModelValidation]
    [Authorize]
    public class UserController : BaseController
    {
        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public UserController(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [Permission(UserPermission.ViewUsers)]
        public ActionResult<ListViewDTO<UserDTO>> GetUsers([FromQuery]ListReuqestDTO<UserListFilterDTO> queryParameters)
        {
            var users = _userRepository.GetAll(queryParameters.Start, queryParameters.Limit, queryParameters.Sort, queryParameters.Filter);

            return new ListViewDTO<UserDTO>
            {
                Data = _mapper.Map<IEnumerable<UserDTO>>(users),
                TotalRecords = _userRepository.Count(queryParameters.Filter)
            };
        }

        [HttpGet("{userId}")]
        [Permission(UserPermission.ViewUsers)]
        public ActionResult<UserDTO> GetUser(int userId)
        {
            var user = _userRepository.GetById(userId);

            if (user == null)
                return NotFound();

            return _mapper.Map<UserDTO>(user);
        }
        
        [HttpDelete("{userId}")]
        [Permission(UserPermission.DeleteUsers)]
        public IActionResult DeleteUser(int userId)
        {
            if (userId == UserId)
                return Forbid();

            var user = _userRepository.Remove(userId);

            if (user == null)
                return NotFound();

            return Ok();
        }

        [HttpPost]
        [Permission(UserPermission.EditUsers)]
        public ActionResult<UserDTO> UpdateUser([FromBody]UserModifyDTO userData)
        {
            var user = _userRepository.GetById(userData.Id);

            if (user == null)
                return NotFound();

            user.Email = userData.Email;
            user.UserName = userData.UserName;

            if (user.Id != UserId)
                user.Permission = userData.Permission;

            if (userData.Password != null)
            {
                var hashedPassword = PasswordHelper.HashPassword(userData.Password);
                user.Password = Encoding.UTF8.GetBytes(hashedPassword);
            }

            var updatedUser = _userRepository.Update(user);

            return _mapper.Map<UserDTO>(updatedUser);
        }

        [HttpPut]
        [Permission(UserPermission.AddUsers)]
        public ActionResult<UserDTO> CreateUser([FromBody]UserCreateDTO userData)
        {
            var hashedPassword = PasswordHelper.HashPassword(userData.Password);
            
            var newUser = new UserEntity
            {
                Email = userData.Email,
                UserName = userData.UserName,
                Password = Encoding.UTF8.GetBytes(hashedPassword)
            };

            _userRepository.Add(newUser);

            return _mapper.Map<UserDTO>(newUser);
        }
    }
}