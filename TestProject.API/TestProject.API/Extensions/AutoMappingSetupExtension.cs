﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using TestProject.API.Mappings;
using TestProject.Application.MappingProfiles;

namespace TestProject.API.Extensions
{
    public static class AutoMappingSetupExtension
    {
        public static void AddAutoMappingSetupSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new EntityToDTOProfile());
                mc.AddProfile(new DTOToEntityProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
