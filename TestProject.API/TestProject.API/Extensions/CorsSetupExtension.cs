﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace TestProject.API.Extensions
{
    public static class CorsSetupExtension
    {
        public static void AddCorsSetup(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            Console.WriteLine(configuration["AppSettings:CorsAllowedOrigins"]);

            var cors = configuration["AppSettings:CorsAllowedOrigins"].Split(';');

            services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    .WithOrigins(cors)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            }));
        }

        public static void UseCorsSetup(this IApplicationBuilder app)
        {
            if (app == null) throw new ArgumentNullException(nameof(app));

            app.UseCors("CorsPolicy");
        }
    }
}
