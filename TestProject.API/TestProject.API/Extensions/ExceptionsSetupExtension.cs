﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using TestProjectApi.Controllers;

namespace TestProject.API.Extensions
{
    public static class ExceptionsSetupExtension
    {
        public static void UseExceptionsSetup(this IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (app == null) throw new ArgumentNullException(nameof(app));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                var logger = loggerFactory?.CreateLogger<AccountController>() ?? throw new ArgumentNullException(nameof(loggerFactory));

                app.UseExceptionHandler(config =>
                {
                    config.Run(async context =>
                    {
                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "application/json";

                        var error = context.Features.Get<IExceptionHandlerFeature>();

                        if (error != null)
                        {
                            var ex = error.Error;
                            logger.LogError(ex.ToString());

                            await context.Response.WriteAsync(new
                            {
                                StatusCode = 500
                            }.ToString());
                        }
                    });
                });
            }
        }
    }
}
