﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using TestProject.Application.Handlers;
using TestProject.Domain.Models;

namespace TestProject.Application.Attributes
{
    public class PermissionAttribute : Attribute, IFilterFactory
    {
        private readonly int _necessaryPermission;

        public PermissionAttribute(UserPermission necessaryPermission)
            => _necessaryPermission = (int)necessaryPermission;

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
            => new PermissionHandler(_necessaryPermission);

        public bool IsReusable => false;
    }
}
