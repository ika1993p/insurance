﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestProject.Application.Attributes
{
    public class StudentAgeValidationAtribute : ValidationAttribute
    {
        private int _age;

        public StudentAgeValidationAtribute(int age)
        {
            _age = age;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var birthday = (DateTime)value;

            DateTime today = DateTime.Today;
            int age = today.Year - birthday.Year;

            if (birthday > today.AddYears(-age))
                age--;

            if (age < _age)
                return new ValidationResult($"Age couldn't be less than {_age}");

            return ValidationResult.Success;
        }
    }
}
