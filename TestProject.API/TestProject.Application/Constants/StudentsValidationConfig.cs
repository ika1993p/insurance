﻿namespace TestProject.Application.Constants
{
    public static class StudentsValidationConfig
    {
        public const string FirstNameRegEx = @"[a-zA-Z]{2,32}";

        public const string LastNameRegEx = @"[a-zA-Z]{5,32}";

        public const string PersonalNumberRegEx = @"^(\d{11})$";

        public const int MinAge = 16;
    }
}
