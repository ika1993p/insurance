﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestProject.Application.DTO.Requests
{
    public class ListReuqestDTO<T> where T : new()
    {
        private int _page;

        [Range(10, 100)]
        public int Limit { get; set; } = 10;

        [Range(0, int.MaxValue)]
        public int Start
        {
            get => _page;
            set => _page = (value - 1) * Limit;
        }

        [MinLength(2), MaxLength(2)]
        public string[] Sort { get; set; } = { "Id", "desc" };

        public T Filter { get; set; } = new T();
    }
}
