﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.Application.DTO.Requests
{
    public class SignInCredentialsDTO
    {
        [Required, EmailAddress, MaxLength(100)]
        public string Email { get; set; }

        [Required, StringLength(35, MinimumLength = 8)]
        public string Password { get; set; }
    }
}
