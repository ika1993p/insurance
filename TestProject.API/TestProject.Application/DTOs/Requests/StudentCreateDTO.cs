﻿using System;
using System.ComponentModel.DataAnnotations;
using TestProject.Application.Attributes;
using TestProject.Application.Constants;
using TestProject.Domain.Models;

namespace TestProject.Application.DTOs.Requests
{
    public class StudentCreateDTO
    {
        [Required, RegularExpression(StudentsValidationConfig.PersonalNumberRegEx)]
        public string PersonalNumber { get; set; }

        [Required, RegularExpression(StudentsValidationConfig.FirstNameRegEx)]
        public string FirstName { get; set; }

        [Required, RegularExpression(StudentsValidationConfig.LastNameRegEx)]
        public string LastName { get; set; }

        [Required, DataType(DataType.Date), StudentAgeValidationAtribute(StudentsValidationConfig.MinAge)]
        public DateTime DateOfBirth { get; set; }

        [Required, EnumDataType(typeof(GenderType))]
        public GenderType Sex { get; set; }
    }
}
