﻿using System;
using System.ComponentModel.DataAnnotations;
using TestProject.Application.Attributes;
using TestProject.Application.Constants;
using TestProject.Domain.Models;

namespace TestProject.Application.DTOs.Requests
{
    public class StudentListFilterDTO
    {
        public int? Id { get; set; }

        [RegularExpression(StudentsValidationConfig.PersonalNumberRegEx)]
        public string PersonalNumber { get; set; }

        [RegularExpression(StudentsValidationConfig.FirstNameRegEx)]
        public string FirstName { get; set; }

        [RegularExpression(StudentsValidationConfig.LastNameRegEx)]
        public string LastName { get; set; }

        [DataType(DataType.Date), StudentAgeValidationAtribute(StudentsValidationConfig.MinAge)]
        public DateTime? DateOfBirth { get; set; }

        [EnumDataType(typeof(GenderType))]
        public GenderType? Sex { get; set; }
    }
}
