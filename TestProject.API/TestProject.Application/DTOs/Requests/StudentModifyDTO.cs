﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.Application.DTOs.Requests
{
    public class StudentModifyDTO : StudentCreateDTO
    {
        [Required]
        public int Id { get; set; }
    }
}
