﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.Application.DTO.Requests
{
    public class UserCreateDTO
    {
        [Required, MinLength(3), MaxLength(32)]
        public string UserName { get; set; }

        [Required, EmailAddress, MaxLength(100)]
        public string Email { get; set; }

        [Required, MinLength(8), MaxLength(32)]
        public string Password { get; set; }
    }
}
