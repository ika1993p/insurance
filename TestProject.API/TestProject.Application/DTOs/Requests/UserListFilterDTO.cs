﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.Application.DTO.Requests
{
    public class UserListFilterDTO
    {
        public int? Id { get; set; }

        [EmailAddress, MaxLength(100)]
        public string Email { get; set; }

        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; }
    }
}
