﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.Application.DTO.Requests
{
    public class UserModifyDTO
    {
        [Required]
        public int Id { get; set; }

        [Required, StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; }

        [Required]
        public short Permission { get; set; }

        [Required, EmailAddress, MaxLength(100)]
        public string Email { get; set; }

        [StringLength(32, MinimumLength = 8)]
        public string Password { get; set; }
    }
}
