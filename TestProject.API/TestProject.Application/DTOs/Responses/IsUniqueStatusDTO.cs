﻿namespace TestProject.Application.DTOs.Responses
{
    public class IsUniqueStatusDTO
    {
        public bool IsUnique { get; set; }
    }
}
