﻿using System;

namespace TestProject.Application.DTOs.Responses
{
    public class StudentDTO
    {
        public int Id { get; set; }

        public string PersonalNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Sex { get; set; }
    }
}
