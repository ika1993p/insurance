﻿namespace TestProject.Application.DTO.Responses
{
    public class UserCredentialsDTO
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string JwtToken { get; set; }

        public short Permission { get; set; }
    }
}
