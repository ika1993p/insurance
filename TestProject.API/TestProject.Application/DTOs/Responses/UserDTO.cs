﻿namespace TestProject.Application.DTO.Responses
{
    public class UserDTO
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public int Permission { get; set; }
    }
}
