﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;
using System.Linq;

namespace TestProject.Application.Handlers
{
    public class PermissionHandler : IAsyncResourceFilter
    {
        private readonly int _necessaryPermission;

        public PermissionHandler(int necessaryPermission) => _necessaryPermission = necessaryPermission;

        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            var permission = context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Permission").Value;
            
            var userPermission = int.Parse(permission);

            if ((userPermission & _necessaryPermission) != 0)
                await next();
            else
                context.Result = new ForbidResult();
        }
    }
}