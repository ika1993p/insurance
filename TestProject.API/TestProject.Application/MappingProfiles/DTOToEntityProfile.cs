﻿using AutoMapper;
using TestProject.Application.DTOs.Requests;
using TestProject.Domain.Entities;

namespace TestProject.Application.MappingProfiles
{
    public class DTOToEntityProfile : Profile
    {
        public DTOToEntityProfile()
        {
            CreateMap<StudentCreateDTO, StudentEntity>();
            CreateMap<StudentModifyDTO, StudentEntity>();
        }
    }
}
