﻿using AutoMapper;
using TestProject.Application.DTO.Responses;
using TestProject.Application.DTOs.Responses;
using TestProject.Domain.Entities;

namespace TestProject.API.Mappings
{
    public class EntityToDTOProfile : Profile
    {
        public EntityToDTOProfile()
        {
            CreateMap<UserEntity, UserDTO>();
            CreateMap<UserEntity, UserCredentialsDTO>();

            CreateMap<StudentEntity, StudentDTO>();
        }
    }
}