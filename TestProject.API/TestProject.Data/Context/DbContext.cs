﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using TestProject.Domain.Entities;
using TestProject.Helpers;

namespace TestProject.Data.Context
{
    public class TestProjectContext : DbContext
    {
        public TestProjectContext(DbContextOptions<TestProjectContext> options) : base(options) { }

        public DbSet<UserEntity> Users { get; set; }

        public DbSet<StudentEntity> Students { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>(entity => {
                entity.HasIndex(e => e.Email).IsUnique();
                entity.HasIndex(e => e.UserName).IsUnique();
            });

            modelBuilder.Entity<StudentEntity>(entity => {
                entity.Property(e => e.PersonalNumber).HasMaxLength(11).IsFixedLength();
                entity.HasIndex(e => e.PersonalNumber).IsUnique();
            });

            modelBuilder.Entity<UserEntity>().HasData(new UserEntity 
            {
                Id = -1,
                Email = "admin@admin.com",
                Permission = 510,
                UserName = "Admin",
                Password = Encoding.UTF8.GetBytes(PasswordHelper.HashPassword("testpassword"))
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
