﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace TestProject.Data.Extensions
{
    public static class DynamicFilterExtension
    {
        public static IQueryable<T> DynamicFilter<T>(this IQueryable<T> queryable, object filter)
        {
            if (filter == null) return queryable;

            foreach (var prop in filter.GetType().GetProperties())
            {
                try
                {
                    object value = prop.GetValue(filter, null);

                    if (value == null)
                        continue;

                    string key = prop.Name;

                    Type type = typeof(T);

                    PropertyInfo property = type.GetProperty(key);

                    ParameterExpression parameter = Expression.Parameter(type, "p");

                    var filterDataType = value.GetType();
                    queryable = queryable.Where(Expression.Lambda<Func<T, bool>>(
                     Expression.Equal(Expression.Convert(Expression.MakeMemberAccess(parameter, typeof(T).GetProperty(key)), filterDataType),
                     Expression.Constant(value)), parameter));
                }
                catch (Exception ex)
                {
                    continue;
                }
            }

            return queryable;
        }
    }
}
