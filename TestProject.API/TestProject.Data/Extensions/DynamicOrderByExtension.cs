﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace TestProject.Data.Extensions
{
    public static class DynamicOrderByExtension
    {
        public static IQueryable<T> DynamicOrderBy<T>(this IQueryable<T> queryable, string[] orderParameters)
        {
            // If there is no order by parameters or order parameters array not contains 2 element (i.e: ["column_name", "desc/asc"]) return qurable
            if (orderParameters.Length == 0 || orderParameters.Length % 2 != 0)
                return queryable;

            try
            {
                string command = orderParameters[1].ToLower() == "desc" ? "OrderByDescending" : "OrderBy";
                Type type = typeof(T);
                PropertyInfo property = type.GetProperty(orderParameters[0].First().ToString().ToUpper() + orderParameters[0].Substring(1));

                ParameterExpression parameter = Expression.Parameter(type, "p");
                MemberExpression propertyAccess = Expression.MakeMemberAccess(parameter, property);
                LambdaExpression orderByExpression = Expression.Lambda(propertyAccess, parameter);
                MethodCallExpression resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType }, queryable.Expression, Expression.Quote(orderByExpression));

                return queryable.Provider.CreateQuery<T>(resultExpression);
            }
            catch (Exception)
            {
                return queryable;
            }
        }
    }
}
