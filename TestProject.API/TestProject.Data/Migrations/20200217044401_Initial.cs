﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestProject.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PersonalNumber = table.Column<string>(fixedLength: true, maxLength: 11, nullable: false),
                    FirstName = table.Column<string>(maxLength: 32, nullable: false),
                    LastName = table.Column<string>(maxLength: 32, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Sex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(maxLength: 32, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    Password = table.Column<byte[]>(maxLength: 32, nullable: false),
                    Permission = table.Column<short>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedDate", "Email", "Password", "Permission", "UserName" },
                values: new object[] { -1, new DateTime(2020, 2, 17, 8, 44, 0, 746, DateTimeKind.Local).AddTicks(2871), "admin@admin.com", new byte[] { 65, 81, 65, 65, 65, 65, 69, 65, 65, 67, 99, 81, 65, 65, 65, 65, 69, 66, 113, 70, 88, 84, 110, 67, 121, 89, 113, 97, 120, 98, 112, 49, 74, 110, 82, 122, 71, 100, 111, 78, 117, 102, 105, 75, 86, 71, 49, 108, 106, 89, 105, 49, 48, 71, 110, 73, 107, 101, 51, 53, 99, 100, 112, 51, 47, 75, 66, 102, 88, 89, 48, 121, 108, 87, 122, 43, 121, 87, 117, 97, 113, 65, 61, 61 }, (short)1022, "Admin" });

            migrationBuilder.CreateIndex(
                name: "IX_Students_PersonalNumber",
                table: "Students",
                column: "PersonalNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserName",
                table: "Users",
                column: "UserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
