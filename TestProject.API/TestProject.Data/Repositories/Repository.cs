﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Extensions;
using TestProject.Domain.Contracts;

namespace TestProject.Data.Repositories
{
    public class Repository<TEntity, TContext> : IRepository<TEntity> where TEntity : class where TContext : DbContext
    {
        protected readonly TContext _context;

        public Repository(TContext context)
        {
            _context = context;
        }

        public TEntity Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();

            return entity;
        }

        public IEnumerable<TEntity> GetAll(int start, int limit, string[] sort, object filter)
        {
            return _context.Set<TEntity>()
                .DynamicFilter(filter)
                .DynamicOrderBy(sort)
                .Skip(start)
                .Take(limit);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public TEntity Remove(int id)
        {
            var entity = _context.Set<TEntity>().Find(id);

            if (entity == null)
                return entity;

            _context.Set<TEntity>().Remove(entity);
            _context.SaveChanges();

            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
            return entity;
        }
        
        public int Count()
        {
            return _context.Set<TEntity>().Count();
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
