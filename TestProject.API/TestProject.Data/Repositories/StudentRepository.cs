﻿using System.Linq;
using TestProject.Data.Context;
using TestProject.Data.Extensions;
using TestProject.Domain.Contracts;
using TestProject.Domain.Entities;

namespace TestProject.Data.Repositories
{
    public class StudentRepository : Repository<StudentEntity, TestProjectContext>, IStudentRepository
    {
        public StudentRepository(TestProjectContext testProjectContext) : base(testProjectContext) { }

        public int Count(object filter)
        {
            return _context.Students
                .DynamicFilter(filter)
                .Count();
        }

        public StudentEntity GetByPersonalNumber(string personalNumber)
        {
            return _context.Students.FirstOrDefault(st => st.PersonalNumber == personalNumber);
        }

        public bool IsPersonalNumberUnique(string personalNumber)
        {
            return GetByPersonalNumber(personalNumber) == null;
        }
    }
}
