﻿using System.Collections.Generic;
using TestProject.Data.Context;
using TestProject.Domain.Contracts;
using TestProject.Domain.Entities;
using System.Linq;
using TestProject.Data.Extensions;

namespace TestProject.Data.Repositories
{
    public class UserRepository : Repository<UserEntity, TestProjectContext>, IUserRepository
    {
        public UserRepository(TestProjectContext testProjectContext) : base(testProjectContext) { }

        public int Count(object filter)
        {
            return _context.Users
                .DynamicFilter(filter)
                .Count();
        }

        public UserEntity GetByEmail(string email)
        {
            return _context.Users.FirstOrDefault(user => user.Email == email);
        }

        public UserEntity GetByUserName(string userName)
        {
            return _context.Users.FirstOrDefault(user => user.UserName == userName);
        }

        public bool IsUserNameUnique(string userName)
        {
            return GetByUserName(userName) == null;
        }

        public bool IsUserEmailUnique(string email)
        {
            return GetByEmail(email) == null;
        }
    }
}
