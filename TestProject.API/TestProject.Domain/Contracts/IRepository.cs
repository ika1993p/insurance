﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProject.Domain.Contracts
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        TEntity Add(TEntity obj);

        TEntity GetById(int id);

        IQueryable<TEntity> GetAll();

        IEnumerable<TEntity> GetAll(int start, int limit, string[] sort, object filter);

        int Count();

        TEntity Update(TEntity obj);

        TEntity Remove(int id);
    }
}
