﻿using System.Collections.Generic;
using TestProject.Domain.Entities;

namespace TestProject.Domain.Contracts
{
    public interface IStudentRepository : IRepository<StudentEntity>
    {
        int Count(object filter);

        bool IsPersonalNumberUnique(string personalNumber);
    }
}
