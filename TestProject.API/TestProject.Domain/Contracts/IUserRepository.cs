﻿using System.Collections.Generic;
using TestProject.Domain.Entities;

namespace TestProject.Domain.Contracts
{
    public interface IUserRepository : IRepository<UserEntity>
    {
        UserEntity GetByEmail(string email);

        bool IsUserNameUnique(string userName);

        bool IsUserEmailUnique(string email);

        int Count(object filter);

        UserEntity GetByUserName(string userName);
    }
}
