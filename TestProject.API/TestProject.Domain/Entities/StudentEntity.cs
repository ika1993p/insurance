﻿using System;
using System.ComponentModel.DataAnnotations;
using TestProject.Domain.Models;

namespace TestProject.Domain.Entities
{
    public class StudentEntity
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(11, MinimumLength = 11)]
        public string PersonalNumber { get; set; }

        [Required, MaxLength(32)]
        public string FirstName { get; set; }

        [Required, MaxLength(32)]
        public string LastName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        public GenderType Sex { get; set; }
    }
}
