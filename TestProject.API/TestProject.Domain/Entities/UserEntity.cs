﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestProject.Domain.Entities
{
    public class UserEntity
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(32)]
        public string UserName { get; set; }

        [Required, MaxLength(100)]
        public string Email { get; set; }

        [Required, MaxLength(32)]
        public byte[] Password { get; set; }

        public short Permission { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
