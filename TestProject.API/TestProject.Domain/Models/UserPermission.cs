﻿namespace TestProject.Domain.Models
{
    public enum UserPermission
    {
        ViewUsers = 2,

        EditUsers = 4,

        AddUsers = 8,

        DeleteUsers = 16,

        ViewStudents = 32,

        EditStudents = 64,

        AddStudents = 128,

        DeleteStudents = 256,
    }
}
